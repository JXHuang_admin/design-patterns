<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Creational\SimpleFactory;

class SimpleFactory {
    public function createBicycle(): Bicycle {
        return new Bicycle();
    }
}