<?php

namespace Hjj\DesignPatterns\Behavioral\ChainOfResponsibilities;

use Psr\Http\Message\RequestInterface;

class SlowStorage extends Handler
{
    protected function processing(RequestINterface $request): ?string
    {
        return 'Hello world';
    }
}