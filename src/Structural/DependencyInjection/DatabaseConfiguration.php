<?php

namespace Hjj\DesignPatterns\Structural\DependencyInjection;

class DatabaseConfiguration
{
    public function __construct(
        protected string $host,
        protected int $port,
        protected string $username,
        protected string $password,
    ) {}

    public function getHost(): string {
        return $this->host;
    }
    public function getPort(): int {
        return $this->port;
    }
    public function getUsername(): string {
        return $this->username;
    }
    public function getPassword(): string {
        return $this->password;
    }
}