<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Tests;

use InvalidArgumentException;

use Hjj\DesignPatterns\Creational\StaticFactory\FormatNumber;
use Hjj\DesignPatterns\Creational\StaticFactory\FormatString;
use Hjj\DesignPatterns\Creational\StaticFactory\StaticFactory;
use PHPUnit\Framework\TestCase;

class StaticFactoryTest extends TestCase {
    public function testCanCreateNumberOfFormatter() {
        $this->assertInstanceOf(FormatNumber::class, StaticFactory::factory('number'));
    }
    public function testCanCreateStringOfFormatter() {
        $this->assertInstanceOf(FormatString::class, StaticFactory::factory('string'));
    }
    public function testException() {
        $this->expectException(InvalidArgumentException::class);
        StaticFactory::factory('object');
    }
}