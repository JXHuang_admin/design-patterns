<?php

namespace Hjj\DesignPatterns\Behavioral\Command;

class HelloCommand implements Command
{
    private Receiver $receiver;
    public function __construct(Receiver $receiver)
    {
        $this->receiver = $receiver;
    }

    public function execute(): void
    {
        $this->receiver->write('Hello World');
    }
}