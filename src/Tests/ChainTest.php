<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Behavioral\ChainOfResponsibilities\FastStorage;
use Hjj\DesignPatterns\Behavioral\ChainOfResponsibilities\Handler;
use Hjj\DesignPatterns\Behavioral\ChainOfResponsibilities\SlowStorage;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;
class ChainTest extends TestCase
{
    private Handler $chain;
    protected function setUp(): void
    {
        $this->chain = new FastStorage(
            ['/foo/bar?index=1' => 'Hello In Memory!'],
            new SlowStorage()
        );
    }

    public function testCanRequestKeyInFastStorage() {
        $uri = $this->createMock(UriInterface::class);
        $uri->method('getPath')->willReturn('/foo/bar');
        $uri->method('getQuery')->willReturn('index=1');

        $request = $this->createMock(RequestInterface::class);
        $request->method('getMethod')->willReturn('GET');
        $request->method('getUri')->willReturn($uri);
        // 匹配到地址，所以直接返回
        $this->assertSame('Hello In Memory!', $this->chain->handle($request));

    }

    public function testCanRequestKeyInSlowStorage() {
        $uri = $this->createMock(UriInterface::class);
        $uri->method('getPath')->willReturn('/foo/bar');
        $uri->method('getQuery')->willReturn('');

        // 没有匹配到地址，找下一个，直到找到最后一个。
        $request = $this->createMock(RequestInterface::class);
        $request->method('getMethod')->willReturn('GET');
        $request->method('getUri')->willReturn($uri);

        $this->assertSame('Hello world', $this->chain->handle($request));

    }
}
