<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Creational\Builder\Parts;

class Truck extends Vehicle {}