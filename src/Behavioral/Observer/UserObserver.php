<?php

namespace Hjj\DesignPatterns\Behavioral\Observer;

use SplObjectStorage;
use SplObserver;
use SplSubject;

class UserObserver implements SplObserver
{
    private array $changeUsers = [];
    public function update(SplSubject $subject): void {
        $this->changeUsers[] = clone $subject;
    }

    public function getChangeUsers() :array {
        return $this->changeUsers;
    }
}