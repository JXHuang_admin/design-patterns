<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Structural\DataMapper\StorageAdapter;
use Hjj\DesignPatterns\Structural\DataMapper\User;
use Hjj\DesignPatterns\Structural\DataMapper\UserMapper;
use PHPUnit\Framework\TestCase;

use InvalidArgumentException;

class DataMapperTest extends TestCase
{
    public function testCanMapUserFromStorage() {
        $storage = new StorageAdapter([1 => [
            'username' => 'someone',
            'email' => 'someone@email.com',
        ]]);
        $mapper = new UserMapper($storage);
        $user = $mapper->findById(1);

        $this->assertInstanceOf(User::class, $user);
    }

    public function testWillNotMapInValidData() {
        $this->expectException(InvalidArgumentException::class);

        $storage = new StorageAdapter([]);
        $mapper = new UserMapper($storage);

        $mapper->findById(1);
    }
}
