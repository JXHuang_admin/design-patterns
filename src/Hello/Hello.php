<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Hello;

class Hello {
    public function Do(): int
    {
        return 1;
    }
}