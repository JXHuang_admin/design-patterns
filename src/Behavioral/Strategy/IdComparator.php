<?php

namespace Hjj\DesignPatterns\Behavioral\Strategy;

use DateTime;

class IdComparator implements Comparator
{
    /**
     * @throws \Exception
     */
    public function compare($a, $b): int
    {
        return $a['id'] <=> $b['id'];
    }
}