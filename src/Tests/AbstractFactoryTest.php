<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Creational\AbstractFactory\CsvWriter;
use Hjj\DesignPatterns\Creational\AbstractFactory\JsonWriter;
use Hjj\DesignPatterns\Creational\AbstractFactory\UnixWriterFactory;
use Hjj\DesignPatterns\Creational\AbstractFactory\WinWriterFactory;
use Hjj\DesignPatterns\Creational\AbstractFactory\WriterFactory;
use PHPUnit\Framework\TestCase;

class AbstractFactoryTest extends TestCase {

    /**
     * @dataProvider provideFactory
     */
    public function testCanCreateCsvWriterOnUnix(WriterFactory $writerFactory)
    {
        $this->assertInstanceOf(JsonWriter::class, $writerFactory->createJsonWriter());
        $this->assertInstanceOf(CsvWriter::class, $writerFactory->createCsvWriter());
    }

    public function provideFactory(): array
    {
        return [
            [new UnixWriterFactory()],
            [new WinWriterFactory()]
        ];
    }
}