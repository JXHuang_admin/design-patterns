<?php

namespace Hjj\DesignPatterns\Structural\FlyWeight;

class word implements Text
{
    private string $name;
    public function __construct(string $name){
        $this->name = $name;
    }

    public function render(string $extrinsicState): string {
        return sprintf('Word %s with font %s', $this->name, $extrinsicState);
    }
}