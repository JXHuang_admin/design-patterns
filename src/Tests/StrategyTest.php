<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Behavioral\Strategy\Context;
use Hjj\DesignPatterns\Behavioral\Strategy\DateComparator;
use Hjj\DesignPatterns\Behavioral\Strategy\IdComparator;
use PHPUnit\Framework\TestCase;

class StrategyTest extends TestCase
{
    public function provideIntegers(): array
    {
        return [
            [
                [
                    ["id" => 2],
                    ['id' => 1],
                    ['id' => 3]
                ],
                ['id' => 1]
            ],
            [
                [
                    ['id' => 3],
                    ['id' => 2],
                    ['id' => 1],
                ],
                ['id' => 1]
            ]
        ];
    }

    public function provideDates(): array
    {
        return [
            [
                [
                    ['date' => '2024-03-03'],
                    ['date' => '2024-03-02'],
                    ['date' => '2024-03-01'],
                ],
                ['date' => '2024-03-01']
            ],
            [
                [
                    ['date' => '2024-03-03'],
                    ['date' => '2024-03-02'],
                    ['date' => '2024-03-01'],
                ],
                ['date' => '2024-03-01']
            ]
        ];
    }

    /**
     * @dataProvider  provideIntegers
     *
     * @param array $collection
     * @param array $expected
    */
    public function testIdComparator(array $collection, array $expected) {
        $obj = new Context(new IdComparator());
        $elements = $obj->executeStrategy($collection);

        $firstElement = array_shift($elements);
        $this->assertSame($expected, $firstElement);
    }

    /**
     * @dataProvider  provideDates
     *
     * @param array $collection
     * @param array $expected
    */
    public function testDateComparator(array $collection, array $expected) {
        $obj = new Context(new DateComparator());
        $elements = $obj->executeStrategy($collection);

        $firstElement = array_shift($elements);
        $this->assertSame($expected, $firstElement);
    }
}
