<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Structural\DataMapper;

class User
{
    public static function fromState(array $state): User {
        return new self(
            $state['username'],
            $state['email'],
        );
    }

    public function __construct(protected string $username, protected string $email)
    {
    }

    public function getUsername(): string {
        return $this->username;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

}