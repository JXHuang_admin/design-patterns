<?php

namespace Hjj\DesignPatterns\Behavioral\Command;

interface Command
{
    public function execute();
}