<?php

namespace Hjj\DesignPatterns\Behavioral\Interpreter;

class AndExp extends AbstractExp
{
    private AbstractExp $first;
    private AbstractExp $second;
    public function __construct(AbstractExp $first, AbstractExp $second)
    {
        $this->first = $first;
        $this->second = $second;
    }

    /**
     * @throws \Exception
     */
    public function interpret(Context $context): bool
    {
        return $this->first->interpret($context) && $this->second->interpret($context);
    }
}