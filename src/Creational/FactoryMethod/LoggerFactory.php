<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Creational\FactoryMethod;

interface LoggerFactory {
    public function createLogger(): Logger;
}