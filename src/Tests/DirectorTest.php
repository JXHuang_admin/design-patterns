<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Creational\Builder\CarBuilder;
use Hjj\DesignPatterns\Creational\Builder\Director;
use Hjj\DesignPatterns\Creational\Builder\Parts\Car;
use Hjj\DesignPatterns\Creational\Builder\Parts\Truck;
use Hjj\DesignPatterns\Creational\Builder\TruckBuilder;
use PHPUnit\Framework\TestCase;

class DirectorTest extends TestCase {
    public function testCanBuildTruck() {
        $truckBuilder = new TruckBuilder();
        $newVehicle = (new Director())->build($truckBuilder);
        $this->assertInstanceOf(Truck::class, $newVehicle);
    }
    public function testCanBuildCar()
    {
        $carBuilder = new CarBuilder();
        $newVehicle = (new Director())->build($carBuilder);
        $this->assertInstanceOf(Car::class, $newVehicle);
    }

}