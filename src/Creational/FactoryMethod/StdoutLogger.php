<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Creational\FactoryMethod;

class StdoutLogger implements Logger {
    public function log(string $message): void
    {
        echo $message;
    }
}