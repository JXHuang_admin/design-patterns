<?php

namespace Hjj\DesignPatterns\Behavioral\Observer;

use SplObjectStorage;
use SplObserver;
use SplSubject;

class User implements SplSubject
{
    private SplObjectStorage $objservers;
    private string $email;

    public function __construct()
    {
        $this->objservers = new SplObjectStorage();
    }

    public function attach(SplObserver $observer): void
    {
        $this->objservers->attach($observer);
    }

    public function detach(SplObserver $observer): void
    {
        $this->objservers->detach($observer);
    }

    public function notify(): void
    {
        foreach($this->objservers as $observer) {
            $observer->update($this);
        }
    }

    public function changeEmail(string $email) :void {
        $this->email = $email;
        $this->notify();
    }
}