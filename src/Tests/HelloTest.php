<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Hello\Hello;
use PHPUnit\Framework\TestCase;

class HelloTest extends TestCase
{

    public function testDo()
    {
        $hello = new Hello();
        $this->assertEquals(1, $hello->do());
    }
}
