<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Structural\Proxy\BankAccountProxy;
use PHPUnit\Framework\TestCase;

class ProxyTest extends TestCase
{
    public function testProxyWillOnlyExecuteExpensiveGetBalanceOnce() {
        $bankAccount = new BankAccountProxy();
        $bankAccount->deposit(30);

        $this->assertSame(30, $bankAccount->getBalance());

        $bankAccount->deposit(50); // 缓存了之前的结果，不会再次执行计算总数。

        $this->assertSame(30, $bankAccount->getBalance());
    }
}
