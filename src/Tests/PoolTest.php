<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Creational\Pool;
use Hjj\DesignPatterns\Creational\Pool\WorkerPool;
use PHPUnit\Framework\TestCase;

class PoolTest extends TestCase
{
    public function testCanGetNewInstanceWithGet() {
        // 新建两个工作对象
        $pool = new WorkerPool();
        $worker1 = $pool->get();
        $worker2 = $pool->get();
        // 查看线池数量，两个工作对象是否同名。
        $this->assertCount(2, $pool);
        $this->assertNotSame($worker1, $worker2);
    }

    public function testCanGetSameInstanceTwiceDisposingItFirst() {
        // 新建一个对象，将该对象设置为空闲状态
        $pool = new WorkerPool();
        $worker1 = $pool->get();
        $pool->dispose($worker1);

        // 再次取对象，会在空闲对象数组中获取到。
        $worker2 = $pool->get();
        // 所以线池中只有一个对象，对象被制空后得到了复用。工作对象1，也就是工作对象2.
        $this->assertCount(1, $pool);
        $this->assertSame($worker1, $worker2);
    }

}
