<?php

namespace Hjj\DesignPatterns\Behavioral\TemplateMethod;

abstract class Journey
{
    private array $thingsToDo = [];

    protected function buyGift(): ?string {
        return null;
    }

    protected function buyAFlight(): string {
        return 'Buy a flight ticket';
    }

    private function takePlane(): string {
        return 'Taking the plane';
    }
    final public function getThingsToDo(): array {
        return $this->thingsToDo;
    }

    final public function takeATrip(): void
    {
        $this->thingsToDo[] = $this->buyAFlight();
        $this->thingsToDo[] = $this->takePlane();
        $this->thingsToDo[] = $this->enjoyVacation();

        $buyGift = $this->buyGift();
        if ($buyGift !== null) {
            $this->thingsToDo[] = $buyGift;
        }
        $this->thingsToDo[] = $this->takePlane();
    }

    abstract protected function enjoyVacation(): string;

}