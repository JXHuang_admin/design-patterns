<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Behavioral\NullObject\NullLogger;
use Hjj\DesignPatterns\Behavioral\NullObject\PrintLogger;
use Hjj\DesignPatterns\Behavioral\NullObject\Service;
use PHPUnit\Framework\TestCase;

class NullObjectTest extends TestCase
{
    public function testNullObject() {
        $service = new Service(new NullLogger());
        $this->expectOutputString('');
        $service->doSomething();
    }

    public function testStandardLogger() {
        $service = new Service(new PrintLogger());
        $this->expectOutputString("We are in Hjj\DesignPatterns\Behavioral\NullObject\Service::doSomething");
        $service->doSomething();
    }

}
