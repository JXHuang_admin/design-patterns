<?php

namespace Hjj\DesignPatterns\Creational\StaticFactory;

interface Formatter
{
    public function format(string $input): string;
}