<?php

namespace Hjj\DesignPatterns\Tests;

use PHPUnit\Framework\TestCase;

class YieldTest extends TestCase
{
//    public function makeRange($length) {
//        $dataSet = [];
//        for ($i=0; $i<$length; $i++) {
//            $dataSet[] = $i;
//        }
//        return $dataSet;
//    }
//    public function testLongFor() {
//        // Time: 00:01.084, Memory: 72.00 MB
//        $customRange = $this->makeRange(1000000);
//        foreach ($customRange as $i) {
//            echo $i . PHP_EOL;
//        }
//        $this->assertSame(1,1);
//    }
//
    public function makeRangeByYield($length) {
        for ($i=0; $i<$length; $i++) {
            yield $i;
        }
    }
    public function testYield() {
        // Time: 00:01.128, Memory: 36.00 MB
        foreach ($this->makeRangeByYield(1000000) as $i) {
            echo $i . PHP_EOL;
        }
        $this->assertSame(1,1);
    }
}
