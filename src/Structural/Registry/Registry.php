<?php

namespace Hjj\DesignPatterns\Structural\Registry;

use InvalidArgumentException;

abstract class Registry
{
    public const LOGGER = 'logger';

    private static array $service = [];

    private static array $allowedKeys = [
        self::LOGGER
    ];

    final public static function set(string $key, Service $value): void
    {
        if (!in_array($key, self::$allowedKeys)) {
            throw new InvalidArgumentException('Invalid key given');
        }
        self::$service[$key] = $value;
    }

    final public static function get(string $key): Service {
        if (!in_array($key, self::$allowedKeys) || !isset(self::$service[$key])) {
            throw new InvalidArgumentException('Invalid key given');
        }
        return self::$service[$key];
    }

}