<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Structural\Composite;

interface Renderable
{
    public function render(): string;
}