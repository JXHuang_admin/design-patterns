<?php

namespace Hjj\DesignPatterns\Structural\FlyWeight;

interface Text
{
    public function render(string $extrinsicState): string;
}