<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Structural\DataMapper;

use InvalidArgumentException;

class StorageAdapter
{
    public function __construct(protected array $data)
    {
    }

    public function find(int $id) {
        if (isset($this->data[$id])) {
            return $this->data[$id];
        }
        return null;
    }
    
}