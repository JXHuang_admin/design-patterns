<?php

namespace Hjj\DesignPatterns\Behavioral\Specification;

class PriceSpecification implements Specification
{
    public function __construct()
    {
    }
    public function isSatisfiedBy(Item $item): bool
    {
        if ($this->maxPrice !== null && $item->getPrice() > $this->maxPrice) {
            return false;
        }
        if ($this->minPrice !== null && $item->getPrice() < $this->minPrice) {
            return false;
        }
        return true;
    }
}