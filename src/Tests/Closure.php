<?php

namespace Hjj\DesignPatterns\Tests;

use PHPUnit\Framework\TestCase;
class App {
    protected $routes = [];
    protected $responseStatus = '200 OK';
    protected $responseContentType = 'text/html';
    protected $responseBody = 'Laravel学院';

    public function addRoute($routePath, $routeCallback) {
        $this->routes[$routePath] = $routeCallback->bindTo($this, __CLASS__);
    }

    public function dispatch($currentPath) {
        foreach ($this->routes as $routePath => $callback) {
            if( $routePath === $currentPath) {
                $callback();
            }
        }
//        header('HTTP/1.1 ' . $this->responseStatus);
//        header('Content-Type: ' . $this->responseContentType);
//        header('Content-Length: ' . mb_strlen($this->responseBody));
        return $this->responseBody;
    }

}
class Closure extends TestCase
{

    public function testClosure () {
        $app = new App();
        $app->addRoute('user/nonfu', function(){
            $this->responseContentType = 'application/json;charset=utf8';
            $this->responseBody = '{"name":"LaravelAcademy"}';
        });
        $result = $app->dispatch('user/nonfu');
        $this->assertSame('{"name":"LaravelAcademy"}', $result);
    }
}