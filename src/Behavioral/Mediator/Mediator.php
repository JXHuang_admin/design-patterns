<?php

namespace Hjj\DesignPatterns\Behavioral\Mediator;

interface Mediator
{
    public function getUser(string $username): string;
}