<?php

namespace Hjj\DesignPatterns\Behavioral\Command;

class AddMessageDateCommand implements UndoableCommand
{
    private Receiver $receiver;
    public function __construct(Receiver $receiver)
    {
        $this->receiver = $receiver;
    }
    public function execute(): void
    {
        $this->receiver->enableDate();
    }
    public function undo(): void
    {
        $this->receiver->disableDate();
    }
}