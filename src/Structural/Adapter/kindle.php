<?php

namespace Hjj\DesignPatterns\Structural\Adapter;

class kindle implements EBook
{

    private int $page = 1;
    private int $totalPages = 100;

    public function pressNext(): void
    {
        $this->page++;
    }

    public function unlock() {

    }

    public function getPage(): array
    {
        return [$this->page, $this->totalPages];
    }
}