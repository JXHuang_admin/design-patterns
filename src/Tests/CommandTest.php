<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Behavioral\Command\HelloCommand;
use Hjj\DesignPatterns\Behavioral\Command\Invoker;
use Hjj\DesignPatterns\Behavioral\Command\Receiver;
use PHPUnit\Framework\TestCase;

class CommandTest extends TestCase
{
    public function testInvocation() {
        $invoker = new Invoker();
        $receiver = new Receiver();

        $invoker->setCommand(new HelloCommand($receiver));
        $invoker->run();
        $this->assertSame('Hello World', $receiver->getOutput());
    }
}
