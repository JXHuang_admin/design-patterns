<?php

namespace Hjj\DesignPatterns\Tests;

use Composite;
use Hjj\DesignPatterns\Structural\Composite\Form;
use Hjj\DesignPatterns\Structural\Composite\InputElement;
use Hjj\DesignPatterns\Structural\Composite\TextElement;
use PHPUnit\Framework\TestCase;

class CompositeTest extends TestCase
{
    public function testRender() {
        $form = new Form();
        $form->addElement(new TextElement('Email:'));
        $form->addElement(new InputElement());
        $embed = new Form();
        $embed->addElement(new TextElement('Password:'));
        $embed->addElement(new InputElement());
        $form->addElement($embed);

        // 这只是例子，浏览器不支持form表单嵌套

        $this->assertSame('<form>Email:<input type="text" /><form>Password:<input type="text" /></form></form>',
            $form->render
        ());
    }
}
