<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Creational\FactoryMethod;

interface Logger {
    public function log(string $message);
}