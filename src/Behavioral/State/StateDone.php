<?php

namespace Hjj\DesignPatterns\Behavioral\State;

class StateDone implements State
{
    public function proceedToNext(OrderContext $context) {
        // end
    }

    public function toString(): string{
        return 'done';
    }
}