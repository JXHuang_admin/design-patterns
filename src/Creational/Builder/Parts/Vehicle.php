<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Creational\Builder\Parts;

class Vehicle {
    final public function setPart(string $key, object $value) {}
}