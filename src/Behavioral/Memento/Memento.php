<?php

namespace Hjj\DesignPatterns\Behavioral\Memento;

class Memento
{
    private State $state;
    public function __construct(State $state)
    {
        $this->state = $state;
    }

    public function getStatue(): State {
        return $this->state;
    }
}