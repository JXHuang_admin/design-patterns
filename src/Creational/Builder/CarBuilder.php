<?php

namespace Hjj\DesignPatterns\Creational\Builder;
use Hjj\DesignPatterns\Creational\Builder\Parts\Car;
use Hjj\DesignPatterns\Creational\Builder\Parts\Door;
use Hjj\DesignPatterns\Creational\Builder\Parts\Engine;
use Hjj\DesignPatterns\Creational\Builder\Parts\Truck;
use Hjj\DesignPatterns\Creational\Builder\Parts\Vehicle;
use Hjj\DesignPatterns\Creational\Builder\Parts\Wheel;

class CarBuilder implements Builder
{
    private Car $car;

    public function addDoors(): void
    {
        $this->car->setPart('rightDoor', new Door());
        $this->car->setPart('leftDoor', new Door());
        $this->car->setPart('trunkLid', new Door());
    }

    public function addEngine(): void
    {
        $this->car->setPart('CarEngine', new Engine());
    }

    public function addWheel(): void
    {
        $this->car->setPart('wheelLF', new Wheel());
        $this->car->setPart('wheelRF', new Wheel());
        $this->car->setPart('wheelLR', new Wheel());
        $this->car->setPart('wheelRR', new Wheel());
    }

    public function createVehicle(): void
    {
        $this->car = new Car();
    }

    public function getVehicle(): Vehicle
    {
        return $this->car;
    }
}