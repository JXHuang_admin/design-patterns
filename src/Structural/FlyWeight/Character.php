<?php

namespace Hjj\DesignPatterns\Structural\FlyWeight;

class Character implements Text
{
    private string $name;
    public function __construct(string $name){
        $this->name = $name;
    }

    public function render(string $extrinsicState): string {
        return sprintf('Character %s with font %s', $this->name, $extrinsicState);
    }
}