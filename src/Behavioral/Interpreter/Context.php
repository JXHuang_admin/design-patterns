<?php

namespace Hjj\DesignPatterns\Behavioral\Interpreter;

use Exception;

class Context
{
    private array $poolVariable;

    /**
     * @throws Exception
     */
    public function lookUp(string $name):bool {
        if (!key_exists($name, $this->poolVariable)) {
            throw new Exception("no exist variable: $name");
        }

        return $this->poolVariable[$name];
    }
    public function assign(VariableExp $variableExp, bool $val): void
    {
        $this->poolVariable[$variableExp->getName()] = $val;
    }
}