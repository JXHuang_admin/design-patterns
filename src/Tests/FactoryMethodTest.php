<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Creational\FactoryMethod\StdoutLogger;
use Hjj\DesignPatterns\Creational\FactoryMethod\StdoutLoggerFactory;
use PHPUnit\Framework\TestCase;

class FactoryMethodTest extends TestCase
{
    public function testCanCreateStdoutLogging() {
        $loggerFactory = new StdoutLoggerFactory();
        $logger=$loggerFactory->createLogger();
        $this->assertInstanceOf(StdoutLogger::class, $logger);
    }
}
