<?php

namespace Hjj\DesignPatterns\Structural\Decorator;

class ExtraBed extends BookingDecorator
{
    private const PRICE = 30;
    public function calculatePrice(): int
    {
        // TODO: Implement calculatePrice() method.
        return $this->booking->calculatePrice() + self::PRICE;
    }
    public function getDescription(): string
    {
        // TODO: Implement getDescription() method.
        return $this->booking->getDescription() . ' with extra bed';
    }
}