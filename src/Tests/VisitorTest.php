<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Behavioral\Visitor\Group;
use Hjj\DesignPatterns\Behavioral\Visitor\RecordingVisitor;
use Hjj\DesignPatterns\Behavioral\Visitor\Role;
use Hjj\DesignPatterns\Behavioral\Visitor\User;
use PHPUnit\Framework\TestCase;

class VisitorTest extends TestCase
{
    private RecordingVisitor $visitor;
    protected function setUp(): void
    {
        $this->visitor = new RecordingVisitor();
    }
    public function provideRoles(): array
    {
        return [
            [
                new User('hjj'),
            ],
            [
                new Group('admin')
            ]
        ];
    }

    /**
     * @dataProvider provideRoles
     */
    public function testVisitSomeRole(Role $role) {
        $role->accept($this->visitor);
        $this->assertSame($role, $this->visitor->getVisited()[0]);
    }
}
