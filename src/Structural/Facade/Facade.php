<?php

namespace Hjj\DesignPatterns\Structural\Facade;


class Facade
{
    public function __construct(protected Bios $bios, protected OperatingSystem $os)
    {
    }

    public function turnOn() {
        $this->bios->execute();
        $this->bios->waitForKeyPress();
        $this->bios->launch($this->os);
    }

    public function turnOff() {
        $this->os->halt();
        $this->bios->powerDown();
    }
}