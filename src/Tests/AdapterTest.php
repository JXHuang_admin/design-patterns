<?php

namespace Hjj\DesignPatterns\Tests;

use Adapter;
use Hjj\DesignPatterns\Structural\Adapter\EBookAdapter;
use Hjj\DesignPatterns\Structural\Adapter\kindle;
use Hjj\DesignPatterns\Structural\Adapter\PaperBook;
use PHPUnit\Framework\TestCase;

class AdapterTest extends TestCase
{
    public function testCanTurnPageOnBook() {
        $book = new PaperBook();
        $book->open();
        $book->turnPage();
        $this->assertSame(2, $book->getPage());
    }

    public function testCanTurnPageOnKindleInANormalBook() {
        $kindle = new kindle();
        $book = new EBookAdapter($kindle); // 套壳进行转换
        $book->open();
        $book->turnPage();

        $this->assertSame(2, $book->getPage());
    }
}
