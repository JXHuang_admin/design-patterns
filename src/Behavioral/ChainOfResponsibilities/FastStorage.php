<?php

namespace Hjj\DesignPatterns\Behavioral\ChainOfResponsibilities;

use Psr\Http\Message\RequestInterface;

class FastStorage extends Handler
{
    public function __construct(protected array $data, ?Handler $successor = null)
    {
        parent::__construct($successor);
    }

    protected function processing(RequestINterface $request): ?string
    {
        $key = sprintf(
            '%s?%s',
            $request->getUri()->getPath(),
            $request->getUri()->getQuery(),
        );
        if ($request->getMethod() === 'GET' && isset($this->data[$key])) {
            return $this->data[$key];
        }
        return null;
    }
}