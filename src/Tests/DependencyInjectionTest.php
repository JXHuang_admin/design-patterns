<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Structural\DependencyInjection\DatabaseConfiguration;
use Hjj\DesignPatterns\Structural\DependencyInjection\DatabaseConnection;
use PHPUnit\Framework\TestCase;

class DependencyInjectionTest extends TestCase
{
    public function testDependencyInjection() {
        // 不管是什么数据库配置，都可以通过 DatabaseConnection 来获取配置信息。
        $config = new DatabaseConfiguration('localhost', 3306, 'user', '1234');

        $connection = new DatabaseConnection($config);

        $this->assertSame('user:1234@localhost:3306', $connection->getDsn());
    }
}
