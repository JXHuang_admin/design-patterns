<?php

namespace Hjj\DesignPatterns\Behavioral\Iterator;

class Book
{
    private string $title;
    private string $author;
    public function __construct(string $title,string $author)
    {
        $this->title = $title;
        $this->author = $author;
    }

    public function getAuthor(): string {
        return $this->author;
    }

    public function getTitle(): string{
        return $this->title;
    }

    public function getAuthorAndTitle():string {
        return $this->getTitle() . ' by ' . $this->getAuthor();
    }
}