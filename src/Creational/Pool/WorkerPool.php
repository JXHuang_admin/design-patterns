<?php

declare(strict_types=1);

namespace Hjj\DesignPatterns\Creational\Pool;
use Countable;
class WorkerPool implements Countable {
    // 两个数组，一个存储活跃的对象，一个存储空闲的对象。
    /**
     * @var StringReverseWorker[]
     */
    private array $freeWorkers = [];
    /**
     * @var StringReverseWorker[]
     */
    private array $occupiedWorkers = [];
    public function get(): StringReverseWorker
    {
        // 没有空闲的对象，则新建一个对象，并存到忙碌对象数组中。
        // 又空闲的对象，则从空闲的对象中取出来使用，并存到忙碌对象数组中。
        if (count($this->freeWorkers) === 0) {
            $worker = new StringReverseWorker();
        } else {
            $worker = array_pop($this->freeWorkers);
        }
        $this->occupiedWorkers[spl_object_hash($worker)] = $worker;
        return $worker;
    }

    public function dispose(StringReverseWorker $worker): void
    {
        // 是否处于忙碌对象数组中，是的话，从忙碌数组中调出，存入到空闲数组中。
        $key=spl_object_hash($worker);
        if (isset($this->occupiedWorkers[$key])) {
            unset($this->occupiedWorkers[$key]);
            $this->freeWorkers[$key] = $worker;
        }
    }

    public function count(): int
    {
        return count($this->occupiedWorkers) + count($this->freeWorkers);
    }
}