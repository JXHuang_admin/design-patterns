<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Creational\SimpleFactory\Bicycle;
use Hjj\DesignPatterns\Creational\SimpleFactory\SimpleFactory;
use PHPUnit\Framework\TestCase;

class SimpleFactoryTest extends TestCase
{
    public function testCanCreateBicycle() {
        $bicycle = (new SimpleFactory())->createBicycle();
        $this->assertInstanceOf(Bicycle::class, $bicycle);
    }
}
