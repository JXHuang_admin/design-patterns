<?php

namespace Hjj\DesignPatterns\Behavioral\Mediator;

abstract class Colleague
{
    protected Mediator $mediator;
    final public function setMediator(Mediator $mediator): void
    {
        $this->mediator = $mediator;
    }
}