<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Structural\FlyWeight\TextFactory;
use PHPUnit\Framework\TestCase;

class FlyWeightTest extends TestCase
{
    private array $characters = [
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
        'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    ];

    private array $fonts = ['Arial', 'Times New Roman', 'Verdana', 'Helvetica'];

    public function testFlyWeight() {
        $factory = new TextFactory();
        for ($i = 0; $i <= 10; $i++) {
            foreach ($this->characters as $char) {
                foreach ($this->fonts as $font) {
                    $flyWeight = $factory->get($char);
                    $rendered = $flyWeight->render($font);
                    $this->assertSame(sprintf('Character %s with font %s', $char, $font), $rendered);
                }
            }
        }

        foreach ($this->fonts as $word) {
            $flyWeight = $factory->get($word);
            $rendered = $flyWeight->render('foobar');

            $this->assertSame(sprintf('Word %s with font foobar', $word), $rendered);
        }

        $this->assertCount(count($this->characters) + count($this->fonts), $factory);
    }
}
