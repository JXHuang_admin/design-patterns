<?php

namespace Hjj\DesignPatterns\Behavioral\ChainOfResponsibilities;

use Psr\Http\Message\RequestInterface;

abstract class Handler
{
    public function __construct(protected ?Handler $successor = null)
    {
    }

    final public function handle(RequestInterface $request): ?string {
        $processed = $this->processing($request);
        // 当第一个为空的时候，调用默认的处理。
        if ($processed === null && $this->successor !== null) {
            $processed = $this->successor->handle($request);
        }
        return $processed;
    }
    abstract protected function processing(RequestINterface $request): ?string;
}