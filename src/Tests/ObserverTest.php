<?php

namespace Hjj\DesignPatterns\Tests;

use Hjj\DesignPatterns\Behavioral\Observer\User;
use Hjj\DesignPatterns\Behavioral\Observer\UserObserver;
use PHPUnit\Framework\TestCase;

class ObserverTest extends TestCase
{
    public function testChangeInUserLeadsToUserObserverBeingNotified() {
        $observer = new UserObserver();

        $user = new User();
        $user->attach($observer);

        $user->changeEmail('foo@bar.com');
        $this->assertCount(1, $observer->getChangeUsers());
    }
}
